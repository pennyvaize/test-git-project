class Base():
    def method():
        print('Hello from Base')


class Child(Base):
    def method():
        print('Hello from Child')


class1 = Base
class1.method()

class2 = Child
class2.method()