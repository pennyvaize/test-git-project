my_list = []
for i in range (1, 10000001):
    my_list.append(i)

"""Создаем генератор отбора квадратов четных чисел из списка """
def generator_square(my_list):
    square_list = []
    for i in my_list:
        if (i**0.5) % 2 == 0:

            square_list.append(i)
    yield square_list


def loop_square(my_list):
    square_list = []
    for i in my_list:
        if (i ** 0.5) % 2 == 0:
            square_list.append(i)
    print(square_list)

#Вызываем генератор

for i in generator_square(my_list):
    print(i)

#Вызываем цикл
loop_square(my_list)
