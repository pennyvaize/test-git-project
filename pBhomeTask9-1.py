import random

num = "nums.txt"
count = 0
sum1 = 0
with open(num, 'w') as file:
    while count < 10000:
        i = str(random.random())
        file.write(i+'\n')
        count += 1


with open(num, 'r') as file:
    for i in file:
        sum1 += float(i)
    print(f' Sum of numbers from {num} is {sum1}')
