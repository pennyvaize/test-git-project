dict1 = {'abc': 11, 'def': 12, 'ghi': 13, 'jkl':14, 'mno':15}

def foo(**kwargs):
    for key, value in kwargs.items():
        print(f'{key} have a value {value}')

foo(**dict1)
foo(prs = 16, tuv = 17)
