
class Degrees():
    def __init__(self, degree):
        self.__degree = degree


    @property
    def degree(self):
        return f'Degree is {self.__degree}°'

    @degree.setter
    def degree(self, degree):
        self.__degree = degree
        print(f'Degree is getting {self.__degree}°')


    def fahrenheit(self):
       fahrenheit = self.__degree*1.8+32
       return f'{self.degree} °C is {fahrenheit:.2f} °F'

    def celsium(self):
        celsium = (self.__degree-32)*5/9
        return f'{self.degree} °F is {celsium:.2f} °C'



cls = Degrees(251)
print(cls.degree)
cls.degree = 451
print(cls.celsium())
print(cls.fahrenheit())
