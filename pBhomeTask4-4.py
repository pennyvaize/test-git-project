class MyException(Exception):
    pass


def game():
    x = float(input('Enter a number lower than 25: '))

    if x < 25:
        print('Have a nice day')
    else:
        raise MyException('This is an error')


try:
    game()
except MyException:
    print('Opps. Try again')
