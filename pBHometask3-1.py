""" Клас моделювання автівки"""

class Car:
    """ Проста спроба змоделювати автівку"""

    def __init__(self, make, model, year):
        """ініціалізувати атрибути, що описують машину."""
        self.make = make
        self.model = model
        self.year = year
        self.__odometr = 0 #Ховаємо одометр, щоб запобігти його змотуванню

    def get_descriptive_name(self):
        """Поернути відформатоване ім'я."""
        long_name = f'{self.year} {self.make} {self.model}'
        return long_name.title()
    @property
    def odometr(self):
        return self.__odometr

    def get_odometr(self):
        return f'My new car has {self.odometr} kilometrs on it'

    def set_odometr(self, kilometrage):

        if kilometrage >= self.__odometr:
            self.__odometr = kilometrage
        else:
            print("You can't roll back an odometr")



a=Car('Audi', 'A6', 2022)
print(a.get_odometr())
print(a.get_descriptive_name())
a.set_odometr(200)
print(a.get_odometr())
a.set_odometr(100)