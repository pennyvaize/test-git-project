text = '''Ground Control to Major Tom Ground Control to Major Tom Take your protein pills and put your helmet on Ground Control to Major Tom Commencing countdown, engines on Check ignition and may God's love be with you Ten, Nine, Eight, Seven, Six, Five, Four, Three, Two, One, Lift off
 This is Ground Control to Major Tom You've really made the grade And the papers want to know whose shirts you wear Now it's time to leave the capsule if you dare'''




def get_split(text):
    """ Создаем функцию, разделяющую текст на слова и очищающую от знаков препинания"""
    list1 = []
    separators = ('.', ',', ';', ':', ' -', ' ')
    for separator in separators:
        text = text.replace(separator, ' ')

    for word in text.split():
        if word not in list1:
            list1.append(word)
    return sorted(list1)

def print_sorted_word(text):
    """Создаем функцию выводящую на печать отсортированный список слов"""
    for word in get_split(text):
        print(word.lower())

print_sorted_word(text)