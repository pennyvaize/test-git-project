class Staff():
    def __init__(self, name, lastname, department, year_of_employment):
        self.name = name
        self.lastname = lastname
        self.department = department
        self.year_of_employment = year_of_employment

        if self.name == "":
            raise ValueError('first name cannot be empty')
        if self.lastname == "":
            raise ValueError('last name cannot be empty')
        if self.department  == "":
            raise ValueError('job title cannot be empty')
        if self.year_of_employment == "":
            raise ValueError('incorrect year')

    def __repr__(self):
        return f"Worker {self.name} {self.lastname} works in {self.department} since {self.year_of_employment}"

    @staticmethod
    def input_worker():
        name = input('Enter a name of worker: ')
        lastname = input('Enter a lastname of worker: ')
        department = input('Enter a department of worker: ')
        year_of_employment = int(input('Enter a year of employment: '))
        return Staff(name, lastname, department, year_of_employment)


def main():
    a = True
    count = None
    while a:
        try:
            count = int(input('Enter a count of staff: '))
            a = False
        except Exception as error:
            print('Error:', error)

    staffs = []
    while len(staffs) < count:
        staff = Staff.input_worker()
        staffs.append(staff)
    print()

    year = int(input('Enter a year: '))
    for staff in staffs:
        if staff.year_of_employment >= year:
            print(staff)


main()
