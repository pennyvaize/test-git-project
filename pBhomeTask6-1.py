""" Создаем функцию отсчитывающую числа в обратном порядке"""
def Desrever(x):
    while x>0:
        yield x
        x -= 1
    return

def space_oddity():
    """Создаем фунецию обратного отсчета"""
    print('''Ground Control to Major Tom
Commencing countdown, engines on
Check ignition and may God's love be with you''')

    so = Desrever(10)
    for x in so:
        print(x)

    print('Lift off')

space_oddity()