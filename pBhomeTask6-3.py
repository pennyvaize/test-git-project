

"""Создаем генератор простых чисел """
def generator_primal(n):
    primal = []
    for i in range(n):
        if i == 2:
            primal.append(i)
        elif i % 2 == 0 or i == 1:
            continue
        else:
            if i == 3 or i == 5 or i == 7:
                primal.append(i)
            else:
                for k in (3, 5, 7):
                    if i % k == 0:
                        break
                else:
                    primal.append(i)
    yield primal


#Вызываем генератор

for i in generator_primal(1000):
    print(i)
