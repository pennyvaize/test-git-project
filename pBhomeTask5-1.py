
iterable = 'It is iterable object'

it = iter(iterable)

try:
    while iterable:
        print(next(it))
except StopIteration:
    print('End of object')