import module_exe


def emulator():
    urls: dict = {}
    while True:
        a = input("""What would you like to do?:
1. Add url.
2. Get url.
Press 1 or 2 to choice or any key to exit: """)

        if a == "1":
            module_exe.set_url(urls)
        elif a == '2':
            module_exe.get_url(urls)
        else:
            choose = input('Would you like to save a urls? Press y or n:')
            if choose == 'y':
                module_exe.write_url(urls)
                print('Your urls is saved')

            print('Have a nice day')
            break
    print("")

emulator()