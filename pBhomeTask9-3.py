import pickle
import json

goods: list= ['book1', 'book2', 'CD', 'audiobook', 'game', 'toy']
file = 'list_of_goods.json'
def save_goods():

    with open(file, 'wb') as f:
        pickle.dump(goods, f)

def load_goods():
    with open(file, 'rb') as f:
        print(pickle.load(f))


save_goods()
load_goods()