"""Задание 3"""


# Значительно эффективнее в данном случае было бы
# использовать set, что вы сможете сделать после
# следующего урока.
class WordSequence(list):
    """Класс, представляющий последовательность слов"""

    _delimiters = ('.', ',', ';', ':', ' -', '- ')

    def __init__(self, text):
        words = [word.casefold() for word in WordSequence._split(text)]
        for word in words:
            if word not in self:
                self.append(word)

    @staticmethod
    def _split(text):
        """Метод разбиения текста. Эффективнее было бы использовать
        модуль re и регулярные выражения, но здесь показано решение
        с использованием изученных средств. В реальном проекте такое
        решение использовать не следует, так как для каждого разделителя
        создаётся новая копия строки."""

        string = text
        for delimiter in WordSequence._delimiters:
            string = string.replace(delimiter, ' ')
        return string.split()


def main():
    text = '''Ground Control to Major Tom Ground Control to Major Tom Take your protein pills and put your helmet on Ground Control to Major Tom Commencing countdown, engines on Check ignition and may God's love be with you Ten, Nine, Eight, Seven, Six, Five, Four, Three, Two, One, Lift off
     This is Ground Control to Major Tom You've really made the grade And the papers want to know whose shirts you wear Now it's time to leave the capsule if you dare'''

    words = WordSequence(text)
    for word in sorted(words):
        print(word)


if __name__ == '__main__':
    main()