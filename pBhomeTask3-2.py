""" Моделювання мов"""

class English():
    def __init__(self, name):
        self.name = name

    def greeting(self):
        print(f'Hello, {self.name}')


class Deutsch():
    def __init__(self, name):
        self.name = name

    def greeting(self):
        print(f'Halo, {self.name}')


first = English ('Sergey')
second = Deutsch ('Sergey')

def hello_friend():
    first.greeting()
    second.greeting()

hello_friend()
