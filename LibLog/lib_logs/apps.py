from django.apps import AppConfig


class LibLogsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lib_logs'
