from django.db import models

# Create your models here.
class Book(models.Model):
    """Назва книги"""
    text = models.CharField(max_length=200)
    data_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Повернути представлення моделі"""
        return self.text

class Review(models.Model):
    """Відгук на прочитану книгу"""
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'reviews'

    def __str__(self):
        return f"{self.text[:50]}..."

