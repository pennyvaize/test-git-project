"""Визначаємо URL шаблони для lib_logs"""
from django.urls import path
from . import views

app_name = 'lib_logs'
urlpatterns = [
    # Головна сторінка
    path('', views.index, name='index'),

    # Сторінка, що виводить всі теми.
    path('books/', views.books, name='books'),

    # Сторінка книги
    path('book/<int:book_id>/', views.book, name='book'),

    # Сторінка для додавання нової теми
    path('new_book/', views.new_book, name= 'new_book'),

    # Сторінка для додавання нової теми
    path('new_review/<int:book_id>/', views.new_review, name='new_review'),

    # Сторінка для додавання нової теми
    path('edit_review/<int:review_id>/', views.edit_review, name='edit_review')
]
