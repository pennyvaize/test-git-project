
def sred(*num):
    first = sum(num)
    second = len(num)
    return first/second

print(sred(2, 3, 5, 8))

print(sred(*range(100)))