"""Программа принимающая две строки и выводящая одинаковые символы """


first = set(input('Enter a first string: ')) # Ввод первой строки и перевод в множество
second = set(input("Enter a second string: ")) # Ввод второй строки и перевод в множество

a = "".join(first.intersection(second)) # Объединение двух сетов и перевод в строку

# Вывод одинаковых символов. Если одинаковых символов нет - появляется сообщение.
if a != "":
    print("")
    print(f'The same letters in first and second string is: ')
    print(a)
else:
    print('No identical letters')
