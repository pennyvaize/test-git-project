import shelve


def emulator():
    urls: dict = {}
    while True:
        a = input("""What would you like to do?:
1. Add url.
2. Get url.
Press 1 or 2 to choice or any key to exit: """)

        if a == "1":
            set_url(urls)
        elif a == '2':
            get_url(urls)
        else:
            print('Have a nice day')
            break
    print("")


def set_url(urls):
    url = input('Enter a url: ')
    short_url = None
    while short_url is None:
        short_url = input('Enter a short name for url: ')
        print("")
        if short_url in urls or short_url is None:
             print('Try one more')
             print("")

    urls[short_url] = url
    with shelve.open('urls_file') as file:
        file=urls

def get_url(urls):
    url_key = input('Enter a short name for url: ')
    url_value = urls.get(url_key, None)
    if url_value:
        print(f' Your url is {url_value}')
        print("")
    else:
        print("This name does not exist. Please try one more")
        print("")


emulator()
