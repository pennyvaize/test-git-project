"""
Пример реализации списка с итератором
"""


class MyList(object):
    """Класс списка"""

    class _ListNode(object):
        """Внутренний класс элемента списка"""

        # По умолчанию атрибуты-данные хранятся в словаре __dict__.
        # Если возможность динамически добавлять новые атрибуты
        # не требуется, можно заранее их описать, что более
        # эффективно с точки зрения памяти и быстродействия, что
        # особенно важно, когда создаётся множество экземляров
        # данного класса.
        __slots__ = ('value', 'prev', 'next')

        def __init__(self, value, prev=None, next=None):
            self.value = value
            self.prev = prev
            self.next = next

        def __repr__(self):
            return 'MyList._ListNode({}, {}, {})'.format(self.value, id(self.prev), id(self.next))

    class _Iterator(object):
        """Внутренний класс итератора"""

        def __init__(self, list_instance):
            self._list_instance = list_instance
            self._next_node = list_instance._head

        def __iter__(self):
            return self

        def __next__(self):
            if self._next_node is None:
                raise StopIteration

            value = self._next_node.value
            self._next_node = self._next_node.next

            return value

    def __init__(self, iterable=None):
        # Длина списка
        self._length = 0
        # Первый элемент списка
        self._head = None
        # Последний элемент списка
        self._tail = None

        # Добавление всех переданных элементов
        if iterable is not None:
            for element in iterable:
                self.append(element)

    def append(self, element):
        """Добавление элемента в конец списка"""

        # Создание элемента списка
        node = MyList._ListNode(element)

        if self._tail is None:
            # Список пока пустой
            self._head = self._tail = node
        else:
            # Добавление элемента
            self._tail.next = node
            node.prev = self._tail
            self._tail = node

        self._length += 1

    def __len__(self):
        return self._length

    def __repr__(self):
        # Метод join класса str принимает последовательность строк
        # и возвращает строку, в которой все элементы этой
        # последовательности соединены изначальной строкой.
        # Функция map применяет заданную функцию ко всем элементам последовательности.
        return 'MyList([{}])'.format(', '.join(map(repr, self)))

    def __get_item_by_index__(self, index):
        if index <0:
            index += len(self)
        if not 0 <= index < len(self):
            raise IndexError('Error: list index out of range')

        node = self._head
        for _ in range(index):
            node = node.next

        return node

    def ins_elem(self, index, value):
        """Добавляем элемента в список"""

        if index < 0:
            index += len(self)

        if index < 0:
            index = 0

        if index >= len(self):
            self.append(value)
        else:
            node = MyList._ListNode(value)

            position = self.__get_item_by_index__(index)

            element_prev = position.prev

            if element_prev is not None:
                element_prev.next = node
            else:
                self._head = node

            position.prev = node

            node.prev = element_prev
            node.next = position

            self._length += 1

    def del_elem(self, index=None):
        """Удаление элемента списка"""

        if index is None:
            index = len(self) - 1

        node = self.__get_item_by_index__(index)

        value = node.value

        prev_elem = node.prev
        next_elem = node.next

        if prev_elem is not None:
            prev_elem.next = next_elem
        else:
            self._head = next_elem

        if next_elem is not None:
            next_elem.prev = prev_elem
        else:
            self._tail = prev_elem

        del node

        self._length -= 1

        return value

    def del_all(self):
        """Удаление списка"""

        node = self._head
        while node:
            next_node = node.next
            del node
            node = next_node

        self._head = self._tail = None
        self._length = 0

    def __getitem__(self, index):
        if not 0 <= index < len(self):
            raise IndexError('list index out of range')

        node = self._head
        for _ in range(index):
            node = node.next

        return node.value

    def __iter__(self):
        return MyList._Iterator(self)


def main():
    my_list = MyList(range(10))
    print(my_list)

    my_list.del_all()
    print(my_list)

    my_list.append(3)
    my_list.ins_elem(2, 3)
    my_list.ins_elem(1, 8)
    print(my_list)

    my_list.del_elem(1)
    print(my_list)

    my_list.del_elem()
    print(my_list)

    my_list.del_elem()
    print(my_list)


if __name__ == '__main__':
    main()